import argparse
from typing import TextIO


def str_sort(string):
    return ''.join(sorted(string))


def get_decoding_map(codes):
    zero_six_nine = []
    two_three_five = []

    zero = None
    one = None
    two = None
    three = None
    four = None
    five = None
    six = None
    seven = None
    eight = None
    nine = None

    # First pass, sort by length. 1, 7, 4, and 8 can be known by length alone. For the rest, we can group them by length
    for code in codes:
        if len(code) == 2:
            one = code
        elif len(code) == 3:
            seven = code
        elif len(code) == 4:
            four = code
        elif len(code) == 5:
            two_three_five.append(code)
        elif len(code) == 6:
            zero_six_nine.append(code)
        elif len(code) == 7:
            eight = code

    # Length 6 - 0, 6, 9
    for code in zero_six_nine:
        # 4 shares a segment with 0 and 6, but not 9
        if len(set(four) - set(code)) == 1:
            # 7 completely overlaps with 0, but not 6
            if len(set(seven) - set(code)) == 0:
                zero = code
            else:
                six = code
        else:
            nine = code

    # Length 5 - 2, 3, 5
    for code in two_three_five:
        # 1 completely overlaps with 3, but not 2 or 5
        if len(set(one) - set(code)) == 0:
            three = code
        else:
            # 6 completely overlaps with 5, but not 2
            if len(set(code) - set(six)) == 0:
                five = code
            else:
                two = code

    # Sort the codes, the order is not guaranteed
    zero = str_sort(zero)
    one = str_sort(one)
    two = str_sort(two)
    three = str_sort(three)
    four = str_sort(four)
    five = str_sort(five)
    six = str_sort(six)
    seven = str_sort(seven)
    eight = str_sort(eight)
    nine = str_sort(nine)

    return {zero: 0, one: 1, two: 2, three: 3, four: 4, five: 5, six: 6, seven: 7, eight: 8, nine: 9}


def get_value(d_map, code):
    return d_map[str_sort(code)]


def part_1(input_file: TextIO):
    count = 0
    for line in input_file.readlines():
        output = line.split('|')[1]
        a, b, c, d = output.split()
        count += 1 if len(a) in [2, 3, 4, 7] else 0
        count += 1 if len(b) in [2, 3, 4, 7] else 0
        count += 1 if len(c) in [2, 3, 4, 7] else 0
        count += 1 if len(d) in [2, 3, 4, 7] else 0
    return count


def part_2(input_file: TextIO):
    total = 0
    for line in input_file.readlines():
        codes, output = line.split('|')
        codes = codes.split()
        a, b, c, d = output.split()
        d_map = get_decoding_map(codes)
        total += int(f'{get_value(d_map, a)}{get_value(d_map, b)}{get_value(d_map, c)}{get_value(d_map, d)}')
    return total


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 8')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
